package com.example.lukaszgronowski.wiki.bindingadapters

import android.databinding.BindingAdapter
import android.view.View
import android.widget.TextView


@BindingAdapter("android:text")
fun setText(view: TextView, value: Int) {
    view.text = value.toString()
}

@BindingAdapter("android:text")
fun setText(view:TextView, value: Double){
    view.text = value.toString()
}

@BindingAdapter("android:visibility")
fun setVisibility(view: View, visible: Boolean) {
    view.visibility = if (visible) View.INVISIBLE else View.VISIBLE
}