package com.example.lukaszgronowski.wiki.di.modules

import com.example.lukaszgronowski.wiki.di.scopes.ActivityScope
import com.example.lukaszgronowski.wiki.model.CitiesModel
import com.example.lukaszgronowski.wiki.model.WeatherModel
import com.example.lukaszgronowski.wiki.navigator.Navigator
import com.example.lukaszgronowski.wiki.repository.WeatherRepository
import com.example.lukaszgronowski.wiki.viewmodel.ErrorHandler
import com.example.lukaszgronowski.wiki.viewmodel.WeatherViewModel
import dagger.Module
import dagger.Provides

@Module
class WeatherModule {

    @Provides
    @ActivityScope
    fun provideWeatherViewModel(
            model: WeatherModel,
            errorHandler: ErrorHandler,
            citiesModel: CitiesModel,
            navigator: Navigator
    ) = WeatherViewModel(
            citiesModel = citiesModel,
            errorHandler = errorHandler,
            navigator = navigator,
            weatherModel = model)

    @Provides
    @ActivityScope
    fun provideWeatherModel(
            repository: WeatherRepository
    ): WeatherModel = WeatherModel(repository)

}