package com.example.lukaszgronowski.wiki.repository

import com.example.lukaszgronowski.wiki.enitity.WeatherData
import com.example.lukaszgronowski.wiki.network.WeatherApi
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class WeatherRepositoryImpl(private val api: WeatherApi) : WeatherRepository {
    override fun getWeatherData(city: String): Observable<WeatherData> {
        return api.getWeatherData(city = city)
                .map { WeatherData(it.main, it.name) }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }
}