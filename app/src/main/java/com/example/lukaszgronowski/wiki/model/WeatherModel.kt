package com.example.lukaszgronowski.wiki.model

import android.databinding.ObservableArrayList
import com.example.lukaszgronowski.wiki.repository.WeatherRepository
import io.reactivex.rxkotlin.toObservable

class WeatherModel(
        private val repository: WeatherRepository
) {

    fun getWeatherData(cities: Set<String>) = cities.toObservable()
            .flatMap(repository::getWeatherData)
            .toList(cities.size)!!
}

internal fun <T> ObservableArrayList<T>.setItem(list: List<T>) {
    clear()
    addAll(list)
}
