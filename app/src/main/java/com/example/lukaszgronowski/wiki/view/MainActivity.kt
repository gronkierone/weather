package com.example.lukaszgronowski.wiki.view

import android.os.Bundle
import com.example.lukaszgronowski.wiki.navigator.Navigator
import dagger.android.DaggerActivity
import javax.inject.Inject

class MainActivity : DaggerActivity() {

    @Inject
    lateinit var navigator: Navigator

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        navigator.startCitiesActivity()
    }
}