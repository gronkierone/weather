package com.example.lukaszgronowski.wiki.view

import android.databinding.DataBindingUtil
import android.os.Bundle
import com.example.lukaszgronowski.wiki.R
import com.example.lukaszgronowski.wiki.databinding.ActivityWeatherBinding
import com.example.lukaszgronowski.wiki.viewmodel.WeatherViewModel
import dagger.android.DaggerActivity
import javax.inject.Inject

class WeatherActivity : DaggerActivity() {
    @Inject
    internal lateinit var viewModel: WeatherViewModel

    val binding by lazy {
        DataBindingUtil.setContentView<ActivityWeatherBinding>(
                this,
                R.layout.activity_weather)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding.viewModel = viewModel
    }

    override fun onDestroy() {
        binding.unbind()
        super.onDestroy()
        viewModel.undbind()
    }

}