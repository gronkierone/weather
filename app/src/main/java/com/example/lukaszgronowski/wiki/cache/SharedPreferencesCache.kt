package com.example.lukaszgronowski.wiki.cache

import android.content.SharedPreferences

class SharedPreferencesCache(
        val name: String,
        val sharedPreferences: SharedPreferences
) : Cache {

    val editor = sharedPreferences.edit()

    override var values: Set<String>
        get() = sharedPreferences.getStringSet(name, emptySet())
        set(value) {
            editor.putStringSet(name, value).apply()
        }
}