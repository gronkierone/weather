package com.example.lukaszgronowski.wiki.di.components

import android.app.Application
import com.example.lukaszgronowski.wiki.AndroidApp
import com.example.lukaszgronowski.wiki.di.modules.ActivityBuilder
import com.example.lukaszgronowski.wiki.di.modules.AppModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = [
    AndroidInjectionModule::class,
    AppModule::class,
    ActivityBuilder::class
])
interface AppComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(app: Application): Builder

        fun build(): AppComponent
    }

    fun inject(app: AndroidApp)
}
