package com.example.lukaszgronowski.wiki.model

import com.example.lukaszgronowski.wiki.cache.Cache
import io.reactivex.subjects.BehaviorSubject

class CitiesModel(
        private val cache: Cache
) {

    val cities = BehaviorSubject.create<Set<String>>()
            .init()

    private fun BehaviorSubject<Set<String>>.init() = apply {
        onNext(cache.values)
        distinctUntilChanged().subscribe(cache::values::set)
    }

    fun addCities(city: Set<String>) = cities.run {
        onNext(city)
    }
}