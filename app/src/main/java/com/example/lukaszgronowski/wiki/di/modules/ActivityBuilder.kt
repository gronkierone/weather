package com.example.lukaszgronowski.wiki.di.modules

import com.example.lukaszgronowski.wiki.di.scopes.ActivityScope
import com.example.lukaszgronowski.wiki.view.CitiesActivity
import com.example.lukaszgronowski.wiki.view.MainActivity
import com.example.lukaszgronowski.wiki.view.WeatherActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuilder {

    @ActivityScope
    @ContributesAndroidInjector(modules = [CitiesModule::class])
    abstract fun bindMainActivity(): MainActivity

    @ContributesAndroidInjector(modules = [CitiesModule::class])
    @ActivityScope
    abstract fun bindCitiesActivity(): CitiesActivity

    @ContributesAndroidInjector(modules = [
        WeatherModule::class,
        CitiesModule::class
    ])
    @ActivityScope
    abstract fun bindWeatherActivity(): WeatherActivity
}