package com.example.lukaszgronowski.wiki.enitity

data class WeatherDesc(
        val temp: Double,
        val pressure: Int,
        val humidity: Int,
        val temp_min: Double,
        val temp_max: Double
)