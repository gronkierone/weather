package com.example.lukaszgronowski.wiki.enitity

data class WeatherData(
        val main: WeatherDesc,
        val name: String
)