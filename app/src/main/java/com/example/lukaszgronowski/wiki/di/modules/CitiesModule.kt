package com.example.lukaszgronowski.wiki.di.modules

import com.example.lukaszgronowski.wiki.cache.Cache
import com.example.lukaszgronowski.wiki.di.scopes.ActivityScope
import com.example.lukaszgronowski.wiki.model.CitiesModel
import com.example.lukaszgronowski.wiki.navigator.Navigator
import com.example.lukaszgronowski.wiki.viewmodel.CitiesViewModel
import dagger.Module
import dagger.Provides

@Module
class CitiesModule {

    @Provides
    @ActivityScope
    fun provideCitiesViewModel(
            citiesModel: CitiesModel,
            navigator: Navigator
    ) = CitiesViewModel(
            citiesModel = citiesModel,
            navigator = navigator
    )

    @Provides
    @ActivityScope
    fun provideCityModel(
            cache: Cache
    ): CitiesModel = CitiesModel(cache = cache)

}