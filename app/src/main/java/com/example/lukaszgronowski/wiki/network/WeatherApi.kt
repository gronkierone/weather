package com.example.lukaszgronowski.wiki.network

import com.example.lukaszgronowski.wiki.BuildConfig
import com.example.lukaszgronowski.wiki.enitity.WeatherData
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

interface WeatherApi {

    @GET("weather")
    fun getWeatherData(
            @Query("q") city: String,
            @Query("units") units: String = BuildConfig.METRIC_QUERY,
            @Query("appid") apiKey : String = BuildConfig.API_KEY
    ): Observable<WeatherData>
}


