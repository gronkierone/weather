package com.example.lukaszgronowski.wiki.di.modules

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import com.example.lukaszgronowski.wiki.BuildConfig
import com.example.lukaszgronowski.wiki.cache.Cache
import com.example.lukaszgronowski.wiki.cache.SharedPreferencesCache
import com.example.lukaszgronowski.wiki.navigator.Navigator
import com.example.lukaszgronowski.wiki.network.WeatherApi
import com.example.lukaszgronowski.wiki.repository.WeatherRepository
import com.example.lukaszgronowski.wiki.repository.WeatherRepositoryImpl
import com.example.lukaszgronowski.wiki.viewmodel.ErrorHandler
import com.example.lukaszgronowski.wiki.viewmodel.ErrorHandlerImpl
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import javax.inject.Singleton


@Module(includes = [ContextModule::class])
class AppModule {



    @Provides
    @Singleton
    fun provideWeatherApi() = Retrofit.Builder()
            .baseUrl(BuildConfig.SERVER_URL)
            .addConverterFactory(MoshiConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build().create(WeatherApi::class.java)


    @Provides
    @Singleton
    fun provideRepository(weatherApi: WeatherApi): WeatherRepository
            = WeatherRepositoryImpl(weatherApi)


    @Provides
    @Singleton
    fun provideErrorHandler(): ErrorHandler
            = ErrorHandlerImpl()

    @Provides
    fun provideSharedPreferences(
            application: Application
    ): SharedPreferences
            = PreferenceManager.getDefaultSharedPreferences(application)

    @Provides
    fun provideCache(
            name: String,
            sharedPreferences: SharedPreferences
    ): Cache = SharedPreferencesCache(
            name = name,
            sharedPreferences = sharedPreferences
    )

    @Provides
    fun provideString(): String = String()

    @Provides
    fun provideNavigator(
            context: Context
    ): Navigator = Navigator(context = context)

}
