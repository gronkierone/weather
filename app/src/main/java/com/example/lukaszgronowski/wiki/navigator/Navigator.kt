package com.example.lukaszgronowski.wiki.navigator

import android.content.Context
import android.content.Intent
import com.example.lukaszgronowski.wiki.di.scopes.ActivityScope
import com.example.lukaszgronowski.wiki.view.CitiesActivity
import com.example.lukaszgronowski.wiki.view.WeatherActivity
import javax.inject.Inject

@ActivityScope
class Navigator @Inject constructor(
        private val context: Context
)  {

    fun startWeatherActivity() {
        context.startActivity(Intent(context, WeatherActivity::class.java))
    }

    fun startCitiesActivity() {
        context.startActivity(Intent(context, CitiesActivity::class.java))
    }
}