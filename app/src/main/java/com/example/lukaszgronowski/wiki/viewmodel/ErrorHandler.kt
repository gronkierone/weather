package com.example.lukaszgronowski.wiki.viewmodel

interface ErrorHandler {
    fun handle(throwable: Throwable)
}