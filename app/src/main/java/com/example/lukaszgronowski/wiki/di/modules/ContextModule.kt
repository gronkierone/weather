package com.example.lukaszgronowski.wiki.di.modules

import android.app.Application
import android.content.Context
import dagger.Module
import dagger.Provides

@Module
object ContextModule {
    @Provides
    @JvmStatic
    fun provideContext(application: Application): Context
            = application.applicationContext
}