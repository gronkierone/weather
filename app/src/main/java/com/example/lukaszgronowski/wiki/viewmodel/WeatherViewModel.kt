package com.example.lukaszgronowski.wiki.viewmodel

import android.databinding.ObservableArrayList
import android.databinding.ObservableField
import com.example.lukaszgronowski.wiki.BR
import com.example.lukaszgronowski.wiki.R
import com.example.lukaszgronowski.wiki.enitity.WeatherData
import com.example.lukaszgronowski.wiki.model.CitiesModel
import com.example.lukaszgronowski.wiki.model.WeatherModel
import com.example.lukaszgronowski.wiki.model.setItem
import com.example.lukaszgronowski.wiki.navigator.Navigator
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.plusAssign
import me.tatarka.bindingcollectionadapter2.ItemBinding

class WeatherViewModel(
        private val weatherModel: WeatherModel,
        private val citiesModel: CitiesModel,
        private val errorHandler: ErrorHandler,
        private val navigator: Navigator
) {
    val weatherItems = ObservableArrayList<WeatherData>()
    val itemBinding: ItemBinding<WeatherData> = ItemBinding.of(BR.item, R.layout.weather_item)
    val progress = ObservableField<Boolean>(false)
    private val compositeDisposable = CompositeDisposable()

    init {
        setVisibility()
        citiesModel.cities.value?.let(this::loadWeather)
    }

    private fun setVisibility(){
        if (weatherItems.isEmpty()) progress.set(true)
    }

    fun refresh() {
        citiesModel.cities.value?.let(this::loadWeather)
    }

    private fun loadWeather(cities: Set<String>) {
        compositeDisposable += weatherModel
                .getWeatherData(cities)
                .subscribe(weatherItems::setItem, errorHandler::handle)
    }

    fun backToCities() {
        navigator.startCitiesActivity()
    }

    fun undbind() {
        compositeDisposable.clear()
    }
}