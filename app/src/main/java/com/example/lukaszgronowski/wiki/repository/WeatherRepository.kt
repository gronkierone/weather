package com.example.lukaszgronowski.wiki.repository

import com.example.lukaszgronowski.wiki.enitity.WeatherData
import com.example.lukaszgronowski.wiki.enitity.WeatherDesc
import io.reactivex.Observable

interface WeatherRepository {
    fun getWeatherData(searchedPhrase: String): Observable<WeatherData>
}