package com.example.lukaszgronowski.wiki.view

import android.databinding.DataBindingUtil
import android.os.Bundle
import com.example.lukaszgronowski.wiki.BR
import com.example.lukaszgronowski.wiki.R
import com.example.lukaszgronowski.wiki.databinding.ActivityCityBinding
import com.example.lukaszgronowski.wiki.viewmodel.CitiesViewModel
import dagger.android.DaggerActivity
import me.tatarka.bindingcollectionadapter2.ItemBinding
import javax.inject.Inject

class CitiesActivity : DaggerActivity() {

    @Inject
    lateinit var citiesViewModel: CitiesViewModel

    val binding by lazy {
        DataBindingUtil.setContentView<ActivityCityBinding>(
                this,
                R.layout.activity_city)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding.viewModel = citiesViewModel
    }

    override fun onDestroy() {
        binding.unbind()
        super.onDestroy()
    }

}