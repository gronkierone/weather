package com.example.lukaszgronowski.wiki.viewmodel

import android.databinding.ObservableArrayList
import android.databinding.ObservableField
import com.example.lukaszgronowski.wiki.BR
import com.example.lukaszgronowski.wiki.R
import com.example.lukaszgronowski.wiki.model.CitiesModel
import com.example.lukaszgronowski.wiki.navigator.Navigator
import me.tatarka.bindingcollectionadapter2.ItemBinding

class CitiesViewModel(
        private val citiesModel: CitiesModel,
        private val navigator: Navigator
) {

    val cityField = ObservableField<String>()
    val itemBinding: ItemBinding<String> = ItemBinding.of(BR.item, R.layout.item_city)
    val cityItems = ObservableArrayList<String>()

    fun addCity() {
        cityField.get().let { cityItems.add(it) }
        cityField.set(EMPTY_STRING)
    }

    fun saveCities() {
        cityItems.asIterable().toSortedSet().let(citiesModel::addCities)
        navigator.startWeatherActivity()
    }

    private companion object {
       const val EMPTY_STRING = ""
    }

}
