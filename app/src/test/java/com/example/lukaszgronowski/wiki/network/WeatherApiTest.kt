package com.example.lukaszgronowski.wiki.network

import com.example.lukaszgronowski.wiki.BuildConfig
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import org.junit.Test

import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

class WeatherApiTest {

    fun createRetrofit() = Retrofit.Builder()
            .baseUrl(BuildConfig.SERVER_URL)
            .addConverterFactory(MoshiConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build().create(WeatherApi::class.java)


    @Test
    fun getWeatherData() {
        createRetrofit()
                .getWeatherData("Warsaw")
                .test()
                .assertNoErrors()
                .assertValueCount(1)
    }
}