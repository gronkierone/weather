package com.example.lukaszgronowski.wiki.model

import com.example.lukaszgronowski.wiki.cache.SharedPreferencesCache
import io.mockk.MockKAnnotations
import io.mockk.every
import io.mockk.impl.annotations.MockK
import org.junit.Before
import org.junit.Test

class CitiesModelTest {

    lateinit var citiesModel: CitiesModel

    @MockK
    lateinit var cache: SharedPreferencesCache

    @Before
    fun setup() {
        MockKAnnotations.init(this, relaxUnitFun = true)
        every { cache.values } returns CITIES
        citiesModel = CitiesModel(cache)

    }

    @Test
    fun addCities() {
        val citiesModelTest = citiesModel.cities.test()

        citiesModel.addCities(CITIES)

        citiesModelTest.assertValue(CITIES)
    }

    private companion object {
        val CITIES = setOf("London", "Paris", "Warsaw")
    }

}