package com.example.lukaszgronowski.wiki.model

import com.example.lukaszgronowski.wiki.enitity.WeatherData
import com.example.lukaszgronowski.wiki.enitity.WeatherDesc
import com.example.lukaszgronowski.wiki.repository.WeatherRepository
import io.mockk.MockKAnnotations
import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.reactivex.Observable
import org.junit.Before
import org.junit.Test


class WeatherModelTest {

    @MockK
    lateinit var repository: WeatherRepository
    lateinit var model: WeatherModel

    @Before
    fun setup() {
        MockKAnnotations.init(this)
        every { repository.getWeatherData(any()) } returns Observable.just(TEST_DATA)
        model = WeatherModel(repository)
    }

    @Test
    fun test() {
        val cities = setOf(CITY)
        val weatherDataList = listOf(TEST_DATA)
        val testWeatherData = model.getWeatherData(cities).test()
        testWeatherData.assertValue(weatherDataList)
    }

    companion object {
        private val CITY = "London"
        private val TEST_DATA = WeatherData(name = CITY, main = WeatherDesc(20.0, 918, 50, 21.0, 22.0))
    }

}

